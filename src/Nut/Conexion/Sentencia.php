<?php

namespace Nut\Conexion;

class Sentencia {

    private $sql;
    private $opciones = [];
    private $tabla = [];
    private $campos = [];
    private $valores = [];
    private $condiciones = [];
    private $grupos = [];
    private $filtros = [];
    private $ordenados = [];
    private $limite = [];
    public $prefijoAlias = 't';

    public function __construct($datos = []) {
        foreach ($datos as $atributo => $valor) {
            $this->__set($atributo, $valor);
        }
    }

    public function construirSELECT() {
        $this->sql = 'SELECT {OPCIONES} {CAMPOS} FROM {TABLAS} {CONDICIONES} {GRUPOS} {FILTROS} {ORDENADOS} {LIMITE}';
        return $this;
    }

    public function constuirINSERT() {
        $this->sql = 'INSERT {OPCIONES} {TABLA}({CAMPOS}) VALUES ({VALORES})';
        return $this;
    }

    public function construirUPDATE() {
        $this->sql = 'UPDATE {TABLA} SET {ASIGNACIONES} {CONDICIONES}';
        return $this;
    }

    public function construirDELETE() {
        $this->sql = 'DELETE FROM {TABLA} {CONDICIONES}';
        return $this;
    }

    public function construirSQL() {
        $this->sql = str_replace('{OPCIONES}', $this->construirOpciones(), $this->sql);
        $this->sql = str_replace('{CONDICIONES}', $this->construirCondiciones(), $this->sql);
        $this->sql = str_replace('{TABLAS}', $this->construirTablas(), $this->sql);
//        $this->sql = str_replace('{ASIGNACIONES}', $this->construirAsignacion(), $this->sql);
        $this->sql = str_replace('{CAMPOS}', $this->construirCampos(), $this->sql);
        $this->sql = str_replace('{GRUPOS}', $this->construirAgrupamientos(), $this->sql);
        $this->sql = str_replace('{FILTROS}', $this->construirFiltros(), $this->sql);
        $this->sql = str_replace('{ORDENADOS}', $this->construirOrdenados(), $this->sql);
        $this->sql = str_replace('{LIMITE}', $this->construirLimite(), $this->sql);
        return $this->limpiarSQL($this->remplazarTablaPorAlias());
    }

    public function prueba() {
        echo $this->construirSELECT()
          ->construirSQL();
    }

    private function construirAsignacion($campos) {
        $respuesta = [];
        foreach ($campos as $columna) {
            array_push($respuesta, $columna . " = :$columna");
        }
        return implode(', ', $respuesta);
    }

    private function construirOpciones() {
        return implode(' ', $this->opciones);
    }

    private function construirTablas() {
        $tablas = [];
        foreach ($this->tabla as $alias => $tabla) {
            if (preg_match('/\s/', trim($tabla))) {
                $tabla = "($tabla)";
            }
            $tablas[] = "$tabla AS {$this->crearAlias($alias)}";
        }
        return implode(', ', $tablas);
    }

    private function construirCampos() {
        return empty($this->campos) ? '*' : implode(', ', $this->campos);
    }

    private function construirCondiciones() {
        $condicion = $this->arregloComoCadena($this->condiciones);
        if (!empty($condicion)) {
            $condicion = "WHERE $condicion";
        }
        return $condicion;
    }

    public function construirOrdenados() {

    }

    public function construirFiltros() {

    }

    public function construirAgrupamientos() {

    }

    public function construirLimite() {
        $limite = implode(', ', array_slice($this->limite, 0, 2));
        return $limite ? "LIMIT $limite" : '';
    }

    public function arregloComoCadena($arreglo) {
        $cadena = '';
        foreach ($arreglo as $campo) {
            if (is_array($campo)) {
                $cadena .= $this->arregloComoCadena($campo);
            } else {
                $cadena .= $campo;
            }
            $cadena .= ' ';
        }
        return preg_replace('/\(\s*\)/', '', "($cadena)");
    }

    public function limpiarSQL($cadena) {
        $cadenaSinParentesisVacios = preg_replace('/\(\s*\)/', '', $cadena);
        $cadenaSinInterseccionErrada = preg_replace('/\(\s*and\s*(.*)\)/', 'and ($1)', $cadenaSinParentesisVacios);
        $cadenaSinUnionErrada = preg_replace('/\(\s*or\s*(.*)\)/', 'or ($1)', $cadenaSinInterseccionErrada);
        $cadenaSinInterseccionDuplicada = preg_replace('/\s*and\s*(or|and)\s*/', ' and ', $cadenaSinUnionErrada);
        $cadenaSinUnionDuplicada = preg_replace('/\s*or\s*(or|and)\s*/', ' or ', $cadenaSinInterseccionDuplicada);
        $cadenaFormateada = preg_replace('/\s*\)/', ')', $cadenaSinUnionDuplicada);
        if ($cadena != $cadenaFormateada) {
            $cadenaFormateada = $this->limpiarSQL($cadenaFormateada);
        }
        return $cadenaFormateada;
    }

    public function remplazarTablaPorAlias() {
        foreach ($this->tabla as $alias => $tabla) {
            $this->sql = preg_replace("/\s$tabla\./", "{$this->crearAlias($alias)}.", $this->sql);
        }
        return $this->sql;
    }

    public function crearAlias($alias) {
        if (is_numeric($alias)) {
            $alias = "$this->prefijoAlias$alias";
        }
        return $alias;
    }

    public function __get($atributo) {
        return $this->$atributo;
    }

    public function __set($atributo, $valor) {
        if (is_array($this->$atributo) and is_scalar($valor)) {
            $valor = [$valor];
        }
        $this->$atributo = $valor;
    }

}
