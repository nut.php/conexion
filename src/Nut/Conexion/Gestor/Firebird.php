<?php

namespace Nut\Conexion\Gestor;

use Nut\Conexion\{
    BaseDatos,
    Proveedor\Pdo,
    Sistema,
    Excepcion\PDO\Firebird as Excepcion
};

/**
 * Descripcion de Firebird
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Firebird extends Pdo {

    /**
     *
     * @var \Nut\Conexion\Proveedor
     */
    private $instancia;

    public function __construct(BaseDatos $baseDatos) {
        try {
            if (Sistema::esWindows()) {
                $dsn = "firebird:dbname=$baseDatos->nombre;charset=$baseDatos->codificacion;host=$baseDatos->hospedaje;port=3050";
            } else {
                $dsn = "firebird:dbname=$baseDatos->hospedaje/3050:/$baseDatos->nombre;charset=$baseDatos->codificacion";
            }
            parent::__construct($dsn, $baseDatos->usuario, $baseDatos->contraseña, null);
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    protected function procederError($excepcion) {
        throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
    }

    public function contarFilas() {
        try {
            $retorno = parent::contarFilas();
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function ejecutar($Sentencia) {
        try {
            $retorno = parent::ejecutar($Sentencia);
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function guardarCambios() {
        try {
            $retorno = parent::guardarCambios();
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function iniciarTransaccion() {
        try {
            $retorno = parent::iniciarTransaccion();
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function insertar($Sentencia) {
        try {
            $retorno = parent::insertar($Sentencia);
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function obtenerUltimoId() {
        try {
            $retorno = parent::obtenerUltimoId();
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function revertirCambios() {
        try {
            $retorno = parent::revertirCambios();
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function seleccionar($Sentencia) {
        try {
            $retorno = parent::seleccionar($Sentencia);
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function seleccionarTodo($Sentencia) {
        try {
            $retorno = parent::seleccionarTodo($Sentencia);
        } catch (\PDOException $excepcion) {
            $retorno = $this->procederError($excepcion);
        }
        return $retorno;
    }

    public function __get($atributo) {
        return $this->instancia->$atributo;
    }

    public function __call($metodo, $argumentos) {
        return call_user_func_array([$this->instancia, $metodo], $argumentos);
    }

}
