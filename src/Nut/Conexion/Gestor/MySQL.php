<?php

namespace Nut\Conexion\Gestor;

use Nut\Conexion\{
    Proveedor\Mysqli,
    Proveedor\Pdo,
    BaseDatos
};

/**
 * Descripcion de MySQL
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class MySQL {

    /**
     *
     * @var \Nut\Conexion\Proveedor|\PDO|\mysqli
     */
    private $instancia;

    public function __construct(BaseDatos $baseDatos) {
        switch ($baseDatos->opciones['proveedor']) {
            case 'PDO':
                $this->instancia = $this->instanciarPdo($baseDatos);
                break;
            case 'mysqli':
                $this->instancia = $this->instanciarMysqli($baseDatos);
                break;
            default:
                throw new \Exception("No se ha podido encontrar el proveedor especificado ({$baseDatos->opciones['proveedor']}).");
        }
    }

    public function instanciarPdo(BaseDatos $baseDatos) {
        return new Pdo(
          "mysql:dbname=$baseDatos->nombre;host=$baseDatos->hospedaje"
          , $baseDatos->usuario
          , $baseDatos->contraseña
          , array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '$baseDatos->codificacion'")
        );
    }

    public function instanciarMysqli(BaseDatos $baseDatos) {
        return new Mysqli(
          $baseDatos->hospedaje
          , $baseDatos->usuario
          , $baseDatos->contraseña
          , $baseDatos->nombre
        );
    }

    public function __get($atributo) {
        return $this->instancia->$atributo;
    }

    public function __call($metodo, $argumentos) {
        return call_user_func_array([$this->instancia, $metodo], $argumentos);
    }

}
