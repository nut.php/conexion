<?php

namespace Nut\Conexion;

/**
 * Descripcion de BaseDato
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class BaseDatos {

    const GESTOR_MYSQL = 0;
    const GESTOR_POSTGRES = 1;
    const GESTOR_FIREBIRD = 2;

    public $hospedaje;
    public $puerto;
    public $nombre;
    public $usuario;
    public $contraseña;
    public $codificacion;
    public $opciones;
    private $gestor;

    public function __construct(array $configuracion = []) {
        $this->modificarConfiguracionSegunGestor(
          $configuracion, $configuracion['gestor'] ?? self::GESTOR_MYSQL
        );
    }

    private function configuracionMysql(array $configuracion) {
        $this->hospedaje = $configuracion['hospedaje'] ?? $this->hospedaje ?? 'localhost';
        $this->puerto = $configuracion['puerto'] ?? $this->puerto ?? 3306;
        $this->nombre = $configuracion['nombre'] ?? $this->nombre ?? null;
        $this->usuario = $configuracion['usuario'] ?? $this->usuario ?? 'root';
        $this->contraseña = $configuracion['contraseña'] ?? $this->contraseña ?? null;
        $this->codificacion = $configuracion['codificacion'] ?? $this->codificacion ?? 'utf8';
        $this->opciones = $configuracion['opciones'] ?? $this->opciones ?? [];
        $this->opciones['proveedor'] = $this->opciones['proveedor'] ?? 'PDO';
    }

    public function configuracionPostgres(array $configuracion) {
        $this->hospedaje = $configuracion['hospedaje'] ?? $this->hospedaje ?? 'localhost';
        $this->puerto = $configuracion['puerto'] ?? $this->puerto ?? 5432;
        $this->nombre = $configuracion['nombre'] ?? $this->nombre ?? null;
        $this->usuario = $configuracion['usuario'] ?? $this->usuario ?? 'postgres';
        $this->contraseña = $configuracion['contraseña'] ?? $this->contraseña ?? null;
        $this->codificacion = $configuracion['codificacion'] ?? $this->codificacion ?? 'utf8';
        $this->opciones = $configuracion['opciones'] ?? $this->opciones ?? [];
    }

    public function configuracionFirebird(array $configuracion) {
        $this->hospedaje = $configuracion['hospedaje'] ?? $this->hospedaje ?? 'localhost';
        $this->puerto = $configuracion['puerto'] ?? $this->puerto ?? 3050;
        $this->nombre = $configuracion['nombre'] ?? $this->nombre ?? null;
        $this->usuario = $configuracion['usuario'] ?? $this->usuario ?? 'SYSDBA';
        $this->contraseña = $configuracion['contraseña'] ?? $this->contraseña ?? 'masterkey';
        $this->codificacion = $configuracion['codificacion'] ?? $this->codificacion ?? 'utf8';
        $this->opciones = $configuracion['opciones'] ?? $this->opciones ?? [];
    }

    private function modificarConfiguracionSegunGestor($configuracion, $gestor) {
        switch ($gestor) {
            case self::GESTOR_MYSQL:
                $this->configuracionMysql($configuracion);
                break;
            case self::GESTOR_POSTGRES:
                $this->configuracionPostgres($configuracion);
                break;
            case self::GESTOR_FIREBIRD:
                $this->configuracionFirebird($configuracion);
                break;
            default:
                throw new \Exception('No se ha podido establecer un gestor especificado');
        }
        $this->gestor = $gestor;
    }

    public function __set($atributo, $valor) {
        if ($atributo == 'gestor') {
            $this->modificarConfiguracionSegunGestor($valor);
        }
    }

    public function __get($atributo) {
        return $this->$atributo;
    }

}
