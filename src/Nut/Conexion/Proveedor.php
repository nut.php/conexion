<?php

namespace Nut\Conexion;

interface Proveedor {

    function iniciarTransaccion();

    function guardarCambios();

    function revertirCambios();

    function ejecutar($Sentencia);

    function insertar($Sentencia);

    function seleccionarTodo($Sentencia);

    function seleccionar($Sentencia);

    function obtenerUltimoId();

    function contarFilas();
}
