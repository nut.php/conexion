<?php

namespace Nut\Conexion\Excepcion\PDO;

use Nut\Conexion\Excepcion;

/**
 * Descripcion de MySQL
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class MySQL extends \PDOException implements Excepcion {

    public function __construct(string $message = "", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, (int) $code, $previous);
    }

}
