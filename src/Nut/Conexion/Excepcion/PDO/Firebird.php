<?php

namespace Nut\Conexion\Excepcion\PDO;

/**
 * Descripcion de Firebird
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Firebird extends \PDOException implements \Nut\Conexion\Excepcion {

    public function __construct(string $message = "", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, (int) $code, $previous);
    }

}
