<?php

namespace Nut\Conexion\Excepcion;

use Nut\Conexion\Excepcion;

/**
 * Descripcion de Mysqli
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Mysqli extends \Exception implements Excepcion {

    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
