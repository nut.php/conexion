<?php

namespace Nut\Conexion;

/**
 * Descripcion de Sistema
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Sistema {

    public static function esWindows() {
        $so = php_uname('s');
        return stripos($so, 'win') !== false;
    }

}
