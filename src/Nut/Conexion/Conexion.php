<?php

namespace Nut\Conexion;

use Nut\Conexion\{
    BaseDatos,
    Gestor\MySQL
};

/**
 * Descripcion de Conexion
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Conexion {

    /**
     *
     * @var \Nut\Conexion\Proveedor|\PDO|\mysqli
     */
    private $instancia;

    /**
     *
     * @param BaseDatos $baseDatos
     * @return Proveedor
     * @throws Exception
     */
    public function __construct(BaseDatos $baseDatos) {
        switch ($baseDatos->gestor) {
            case BaseDatos::GESTOR_MYSQL:
                $this->instancia = new MySQL($baseDatos);
            case BaseDatos::GESTOR_POSTGRES:
                break;
            case BaseDatos::GESTOR_FIREBIRD:
                $this->instancia = new Gestor\Firebird($baseDatos);
                break;
            default :
                throw new Exception('No es posible establecer conexion con el gestor solicitado.');
        }
    }

    /**
     *
     * @param string $atributo
     * @return mixed
     */
    public function __get($atributo) {
        return $this->instancia->$atributo;
    }

    public function __call($metodo, $argumentos) {
        return call_user_func_array([$this->instancia, $metodo], $argumentos);
    }

}
