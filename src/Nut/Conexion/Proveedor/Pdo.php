<?php

namespace Nut\Conexion\Proveedor;

use Nut\Conexion\{
    Proveedor,
    Sentencia,
    Excepcion\PDO\MySQL as Excepcion
};

/**
 * Descripcion de Pdo
 *
 * @author Neder Fandiño <nekoOs.com>
 * @date 20/07/2016
 * @time 02:24:19 AM
 */
class Pdo extends \PDO implements Proveedor {

    /**
     *
     * @var Sentencia
     */
    private $sentencia;

    /**
     * Inialización del constructor \PDO
     * @param string $dsn
     * @param string $usuario
     * @param string $contraseña
     * @param array $opciones
     * @throws Excepcion\PDO\MySQL
     */
    public function __construct($dsn, $usuario = null, $contraseña = null, array $opciones = null) {
        try {
            parent::__construct($dsn, $usuario, $contraseña, $opciones);
            /* Habilitar la captura de errores */
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     * Permite iniciar una transaccion en Base de Datos
     * @return boolean
     */
    function iniciarTransaccion() {
        return $this->beginTransaction();
    }

    /**
     * Permite guardar los cambios en una transaccion de Base de Datos
     * @return boolean
     */
    public function guardarCambios() {
        return $this->commit();
    }

    /**
     * Permite revertir los cambios realidos en una transaccion de Base de Datos
     * @todo Implementar un unico metodo para revertir cambios en caso de errores
     * @return boolean
     */
    public function revertirCambios() {
        return $this->rollBack();
    }

    /**
     *
     * @param mixin $datos
     * @return bool
     * @throws Excepcion\PDO\MySQL
     */
    public function ejecutar($datos) {
        try {
            $Sentencia = $this->construirSentencia($datos);
            $this->sentencia = $this->prepare($Sentencia->sql);
            return $this->sentencia->execute($Sentencia->valores);
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     *
     * @param mixin $datos
     * @return int
     * @throws Excepcion\PDO\MySQL
     */
    public function insertar($datos) {
        try {
            $Sentencia = $this->construirSentencia($datos);
            $this->sentencia = $this->prepare($Sentencia->sql);
            $this->sentencia->execute($Sentencia->valores);
            return $this->sentencia->rowCount();
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     *
     * @param mixin $datos
     * @return object
     * @throws Excepcion\PDO\MySQL
     */
    public function seleccionarTodo($datos) {
        try {
            $Sentencia = $this->construirSentencia($datos);
            $this->sentencia = $this->prepare($Sentencia->sql);
            $this->sentencia->execute($Sentencia->valores);
            return $this->sentencia->fetchAll(\PDO::FETCH_OBJ);
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     *
     * @param mixin $datos
     * @return object
     * @throws Excepcion\PDO\MySQL
     */
    public function seleccionar($datos) {
        try {
            $Sentencia = $this->construirSentencia($datos);
            $this->sentencia = $this->prepare($Sentencia->sql);
            $this->sentencia->execute($Sentencia->valores);
            return $this->sentencia->fetch(\PDO::FETCH_OBJ);
        } catch (\PDOException $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     *
     * @return int
     */
    public function obtenerUltimoId() {
        return $this->lastInsertId();
    }

    /**
     *
     * @return int
     */
    public function contarFilas() {
        return $this->sentencia->rowCount();
    }

    /**
     *
     * @param mixin $datos
     * @return Sentencia
     * @throws Excepcion\PDO\MySQL
     */
    private function construirSentencia($datos) {
        $Sentencia = $datos;
        if (is_string($datos)) {
            $Sentencia = new Sentencia(["sql" => $datos]);
        } elseif (is_array($datos) or is_object($datos)) {
            $Sentencia = new Sentencia($Sentencia);
        }
        if ($Sentencia instanceof Sentencia) {
            return $Sentencia;
        }
        throw new Excepcion('El tipo de dato no es compatible con una Sentencia...');
    }

}
