<?php

namespace Nut\Conexion\Proveedor;

use Nut\Conexion\{
    Proveedor,
    Sentencia,
    Excepcion\Mysqli as Excepcion
};

mysqli_report(MYSQLI_REPORT_STRICT);

/**
 * Descripcion de Mysqli
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Mysqli extends \mysqli implements Proveedor {

    /**
     * Inicialización del constructor \mysqli
     * @extends \mysqli
     * @param string $hospedaje
     * @param string $usuario
     * @param string $contraseña
     * @param string $base_datos
     */
    public function __construct($hospedaje, $usuario, $contraseña, $base_datos) {
        try {
            parent::__construct($hospedaje, $usuario, $contraseña, $base_datos);
        } catch (\mysqli_sql_exception $excepcion) {
            throw new Excepcion($excepcion->getMessage(), $excepcion->getCode(), $excepcion);
        }
    }

    /**
     * Permite iniciar una transaccion en Base de Datos
     * @todo Implementar un unico metodo para la iniciacion de la transaccion
     */
    public function iniciarTransaccion() {
        $this->autocommit(false);
        $this->begin_transaction(MYSQLI_TRANS_START_READ_ONLY);
        $this->query("START TRANSACTION");
    }

    /**
     * Permite guardar los cambios en una transaccion de Base de Datos
     */
    public function guardarCambios() {
        $this->commit();
    }

    /**
     * Permite revertir los cambios realidos en una transaccion de Base de Datos
     * @todo Implementar un unico metodo para revertir cambios en caso de errores
     */
    public function revertirCambios() {
        $this->rollback();
        $this->query("ROLLBACK");
    }

    /**
     * Permite ejecutar consultas SQL que no obtienen retorno
     * @param string $sql
     * @param array $valores
     * @param mysqli_stmt $sentencia (ref)
     */
    public function ejecutar(Sentencia $Sentencia) {

    }

    /**
     * Permite realizar consultas del tipo INSERT o UPDATE y retorna el numero de
     * registros afectados
     * @param string $sql
     * @param array $valores
     * @param int $ultimo_id (ref)
     * @return object
     * @throws Exception
     *
     * @todo Hay que corregir aun las excepciones para controlar la falencia de
     *       begin_transaccion para versiones menores a 5.6
     */
    public function insertar(Sentencia $Sentencia, &$ultimo_id = NULL) {

    }

    /**
     * Permite realizar consultas de seleccion obteniendo mas de un registro
     * @param string $sql
     * @param array $valores
     * @param mysqli_stmt $sentencia (ref)
     * @return array
     */
    public function seleccionarTodo(Sentencia $Sentencia) {

    }

    /**
     * Permite realizar consultas de seleccion obteniendo un unico registro
     * @param string $sql
     * @param array $valores
     * @param mysqli_stmt $sentencia (ref)
     * @return object
     */
    public function seleccionar(Sentencia $Sentencia) {

    }

    public function obtenerUltimoId() {

    }

    public function contarFilas() {

    }

}
